package calculator.services;


import static calculator.services.Constant.*;

public class Method {
    public static String method(String operatorStr, double operand1, double operand2) {

        String resultStr = null;
        Double result ;

        switch (operatorStr) {
            case PLUS: {
                result = operand1 + operand2;
                resultStr = result.toString();
                break;
            }
            case MINUS: {
                result = operand1 - operand2;
                resultStr = result.toString();
                break;
            }
            case DIVIDE: {
                if (operand2 == 0) {
                    resultStr = INCORRECT_INPUT;
                } else {
                    result = operand1 / operand2;
                    resultStr = result.toString();
                }
                break;
            }
            case MULTIPLY: {
                result = operand1 * operand2;
                resultStr = result.toString();
                break;
            }
            case REMAINDER_OF_DIVISION: {
                result = operand1 % operand2;
                resultStr = result.toString();
                break;
            }
        }

        return resultStr;

    }}